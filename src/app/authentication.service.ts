import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap } from 'rxjs';
import { Router } from '@angular/router';
import { bootstrapApplication } from '@angular/platform-browser';
import { baseApiUrl } from './shared/constants';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  [x: string]: any;
  private baseApiUrl = 'http://localhost:8000';
  private loginPath = '/api/login';
  private registerPath = '/api/users/new';
  private logoutPath = '/api/logout';
  currentUser = {};

  constructor(private http: HttpClient, public router: Router) {}

  private getUrl(path: string) {
    return this.baseApiUrl + path;
  }
  // Token
  getAccessToken() {
    return localStorage.getItem('pp_access');
  }
  getRefreshToken() {
    return localStorage.getItem('pp_refresh');
  }

  // Sign-up
  public register(email: string, password: string) {
    return this.http
      .post<{
        [x: string]: any;
        error: boolean;
        message: string;
        email?: string;
      }>(this.getUrl(this.registerPath), { email, password })
      .pipe(catchError(this.handleError));
  }

  // Sing-In
  public login(email: string, password: string) {
    // Send login request to beckend
    return this.http
      .post<{ error: boolean; accessToken: string; refreshToken: string }>(
        this.getUrl(this.loginPath),
        { email, password }
      )
      .pipe(catchError(this.handleError));
  }

  // Log-out
  public async logout() {
    localStorage.clear();
    this.router.navigate(['login']);
  }

  // Is authinticated flag
  get isLoggedIn(): boolean {
    let authToken = localStorage.getItem('pp_access');
    return authToken !== null ? true : false;
  }

  // Error
  handleError(error: HttpErrorResponse) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      msg = error.error.message;
    } else {
      // server-side error
      msg =
        error.error.message ||
        `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(() => new Error(msg));
  }
}
