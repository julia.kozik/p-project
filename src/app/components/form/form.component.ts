import { Component, Input, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Note } from 'src/app/shared/types';
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'pm-note-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  error: string = '';

  public subject!: FormControl;
  public url!: FormControl;
  public note!: FormControl;
  public images!: FormControl;

  constructor(public dataService: DataService, public router: Router) {
    // FORM
    this.subject = new FormControl('');
    this.url = new FormControl('');
    this.note = new FormControl('', Validators.required);
    this.images = new FormControl<string[]>([]);
  }
  // Params
  @Input() isEdit: boolean = false;
  @Input() noteData?: Note;

  ngOnInit(): void {}

  ngOnChanges() {
    this.subject?.setValue(this.noteData?.subject);
    this.url?.setValue(this.noteData?.url);
    this.note?.setValue(this.noteData?.note);
    this.images?.setValue(this.noteData?.images);
  }

  // Methods

  addImage() {
    this.images.setValue([
      ...(this.images.value || []),
      'https://picsum.photos/200',
    ]);
    this.handleBlur();
  }

  handleBlur() {
    if (this.isEdit && this.noteData) {
      // Send updates to the BE
      this.dataService
        .updateNoteById(Number(this.noteData.id), {
          subject: this.subject.value,
          url: this.url.value,
          note: this.note.value,
          images: this.images.value,
        })
        .subscribe(
          () => {},
          (error) => {
            this.error = error.message;
          }
        );
    }
  }

  onSubmit() {
    this.dataService
      .addNote(
        this.subject.value,
        this.url.value,
        this.note.value,
        this.images.value
      )
      .subscribe(
        () => {
          this.router.navigate(['/']);
        },
        (error) => {
          this.error = error.message;
        }
      );
  }

  deleteImage(i: number) {
    const images = this.images.value;
    images.splice(i, 1);
    this.images.setValue(images);
    this.handleBlur();
  }
}
