interface NoteBase {
  subject: string;
  url: string;
  note: string;
}

interface NoteMeta {
  id: number;
  createdAt: string;
  updatedAt: string;
}

interface StringImages {
  images: string;
}

interface ArrayImages {
  images: string[];
}

export interface Note extends NoteBase, NoteMeta, ArrayImages {}

export interface NoteResponse extends NoteBase, NoteMeta, StringImages {}

export interface NotePayload extends NoteBase, ArrayImages {}
