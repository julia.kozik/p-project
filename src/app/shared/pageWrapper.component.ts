import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'pm-page-wrapper',
  templateUrl: './pageWrapper.component.html',
  styleUrls: ['./pageWrapper.component.scss']
})
export class PageWrapperComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
