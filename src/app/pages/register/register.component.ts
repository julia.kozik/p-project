import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/authentication.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'pm-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  errorMessage?: string;

  constructor(
    public fb: FormBuilder,
    public authenticationService: AuthenticationService,
    public router: Router
  ) {
    this.registerForm = this.fb.group({
      email: [''],
      password: [''],
    });
  }

  ngOnInit(): void {}

  registerUser() {
    this.authenticationService
      .register(this.registerForm.value.email, this.registerForm.value.password)
      .subscribe((res) => {
        if (res.error) {
          this.errorMessage = res.message;
          return;
        }

        this.registerForm.reset();
        this.router.navigate(['/login']);
      });
  }
}
