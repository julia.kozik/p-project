import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthenticationService } from '../../authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'pm-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  error: string = '';
  constructor(
    public fb: FormBuilder,
    public authenticationService: AuthenticationService,
    public router: Router
  ) {
    this.loginForm = this.fb.group({
      email: [''],
      password: [''],
    });
  }
  ngOnInit() {}

  loginUser() {
    this.authenticationService
      .login(this.loginForm.value.email, this.loginForm.value.password)
      .subscribe(
        (responsFromServer) => {
          localStorage.setItem('pp_access', responsFromServer.accessToken);
          localStorage.setItem(
            'pp_refreshToken',
            responsFromServer.refreshToken
          );
          this.router.navigate(['/']);
        },
        (error) => {
          this.error = error.message;
        }
      );
  }
}
