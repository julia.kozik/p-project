import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NoPreloading, Router } from '@angular/router';
import { DataService } from 'src/app/data.service';
import { Note } from 'src/app/shared/types';

@Component({
  selector: 'pm-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss'],
})
export class NotesComponent implements OnInit {
  public notes: any[] = [];
  error: string = '';
  data: string = '[]';

  activeId: number | undefined;

  public subject!: FormControl;
  public url!: FormControl;
  public note!: FormControl;
  public images!: FormControl;

  private _listFilter: string = '';

  get listFilter(): string {
    return this._listFilter;
  }

  set listFilter(value: string) {
    this._listFilter = value;
    console.log('In setter:', value);
    this.filteredNotes = this.performFilter(value);
  }
  filteredNotes: Note[] = [];

  constructor(public dataService: DataService, public router: Router) {}

  toggleNote(id: number) {
    if (id === this.activeId) {
      return (this.activeId = undefined);
    }
    return (this.activeId = id);
  }

  performFilter(filterBy: string): Note[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this.notes.filter(
      (note: Note) =>
        note.subject.toLocaleLowerCase().includes(filterBy) ||
        note.note.toLocaleLowerCase().includes(filterBy)
    );
  }

  ngOnInit(): void {
    this.getAllNotes();
    // this.listFilter = "";
  }

  handleSerch(event: Event) {
    const input = <HTMLInputElement>event.target;
    this.listFilter = input.value;
  }

  handleSort(param: 'subject' | 'updatedAt' | 'createdAt') {
    const dateFields = ['updatedAt', 'createdAt'];
    const isDate = dateFields.includes(param);

    this.notes.sort((noteA, noteB) => {
      const itemA = isDate
        ? new Date(noteA[param])
        : noteA[param].toLocaleLowerCase();
      const itemB = isDate
        ? new Date(noteB[param])
        : noteB[param].toLocaleLowerCase();

      if (isDate) {
        console.log('A', itemA);
        console.log('B', itemB);

        console.log('=====>>>>', itemA - itemB);
        return itemA - itemB;
      }

      if (itemA < itemB) {
        return -1;
      }
      if (itemA > itemB) {
        return 1;
      }
      return 0;
    });
    if (isDate) {
      this.notes.reverse();
    }

    this.filteredNotes = this.performFilter(this.listFilter);
  }

  hendleDeleteNote(id: number) {
    this.dataService.deleteNoteById(id).subscribe(
      () => {
        this.getAllNotes();
      },
      (error) => {
        this.error = error.message;
      }
    );
  }

  private getAllNotes() {
    this.dataService.getAllNotes().subscribe(
      (response: any) => {
        this.notes = [...response.data];
        this.filteredNotes = [...response.data];
      },
      (error) => {
        this.error = error.message;
      }
    );
  }
}
