import { Component, OnInit } from '@angular/core';
import { Note } from 'src/app/shared/types';
import { DataService } from 'src/app/data.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'pm-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss'],
})
export class UpdateComponent implements OnInit {
  private id?: string;
  public note?: Note;
  public error?: string;

  constructor(
    public dataService: DataService,
    private activeRoute: ActivatedRoute
  ) {
    activeRoute.params
      .pipe(map((p) => p['id']))
      .subscribe((id) => (this.id = id));
  }

  ngOnInit(): void {
    this.dataService.getNoteById(Number(this.id)).subscribe(
      (response) => {
        this.note = {
          ...response.data,
          images: response.data.images ? response.data.images.split(',') : [],
        };
      },
      (error) => {
        this.error = error.message;
      }
    );
  }
}
