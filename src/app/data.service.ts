import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { catchError, throwError } from 'rxjs';

import { Note, NotePayload, NoteResponse } from './shared/types';
import { baseApiUrl } from './shared/constants';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private notesPath = '/api/notes';

  constructor(private http: HttpClient) {}

  private getUrl(path: string) {
    return baseApiUrl + path;
  }

  public addNote(subject: string, url: string, note: string, images: string) {
    // Send note request to beckend
    return this.http
      .post<{
        error: boolean;
        subject: string;
        url: string;
        note: string;
        images: string[];
      }>(this.getUrl(this.notesPath), { subject, url, note, images })
      .pipe(catchError(this.handleError));
  }

  // Get all notes from db

  getAllNotes() {
    return this.http
      .get<{
        error: boolean;
        message: string;
        data: Note[];
      }>(this.getUrl(this.notesPath))
      .pipe(catchError(this.handleError));
  }

  deleteNoteById(id: number) {
    return this.http
      .delete<{ error: boolean; message: string }>(
        this.getUrl(`${this.notesPath}/${id}`)
      )
      .pipe(catchError(this.handleError));
  }

  getNoteById(id: number) {
    return this.http
      .get<{
        error: boolean;
        message: string;
        data: NoteResponse;
      }>(this.getUrl(`${this.notesPath}/${id}`))
      .pipe(catchError(this.handleError));
  }

  //Update
  updateNoteById(id: number, updatedNote: NotePayload) {
    return this.http
      .put<{
        error: boolean;
        message: string;
        data: Note;
      }>(this.getUrl(`${this.notesPath}/${id}`), updatedNote)
      .pipe(catchError(this.handleError));
  }

  // Error
  handleError(error: HttpErrorResponse) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      msg = error.error.message;
    } else {
      // server-side error
      if (error.status === 401) {
        window.localStorage.clear();
        location.reload();
      }
      msg =
        error.error.message ||
        `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(() => new Error(msg));
  }
}
