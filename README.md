# PProject

## Prerecquisites

### Note server
In order to showcase full stack implementation, this project uses BE server and sqlite database to store data.
Get server from https://gitlab.com/julia.kozik/note-server and follow instructions in README.

### Install dependencies
From inside the P-project folder run `npm install`.

## Run
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

## Development server


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.
